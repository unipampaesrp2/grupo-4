package rp2Figuras;

import javax.swing.JOptionPane;

/**
 * Classe com atributos do objeto triangulo.
 *
 * @author Adriano
 */
public class Triangulo {

    private double verticeX1[];
    private double verticeY1[];
    private int numeroLadosTriangulo = 3;
    private String corBordaTriangulo;
    private String corPreenchimentoTriangulo;
    private double valorLado;

    /**
     * Método construtor do objeto triângulo.
     *
     * @param x1 coordenada x do vertice.
     * @param y1 coordenada y do vertice.
     * @param valorLado tamanho do lado do triãngulo.
     */
    public Triangulo(double x1, double y1, double valorLado) {
        this.verticeX1 = new double[3];
        this.verticeY1 = new double[3];
        this.verticeX1[0] = x1;
        this.verticeY1[0] = y1;
        this.valorLado = valorLado;
        calculaVertices();
    }

    /**
     * Método que recebe por parametro a cor da borda da figura e atribui ao
     * objeto.
     *
     * @param corB parametro referente a cor da borda do trinagulo.
     */
    public void setCorBordaTriangulo(String corB) {
        this.corBordaTriangulo = corB;
    }

    /**
     * Método que recebe por parametro a cor do preenchimento da figura e
     * atribui ao objeto.
     *
     * @param corP parametro referente a cor do preenchimento do trinagulo.
     */
    public void setCorPreenchimentoTriangulo(String corP) {
        this.corPreenchimentoTriangulo = corP;
    }

    /**
     * Método que retorna a cor da borda do triangulo.
     *
     * @return
     */
    public String getCorBordaTriangulo() {
        return corBordaTriangulo;
    }

    /**
     * Método que retorna a cor de preenchimento do triangulo.
     *
     * @return
     */
    public String getCorPreenchimentoTriangulo() {
        return corPreenchimentoTriangulo;
    }

    /**
     * Método que retorna o numero de lados do triangulo.
     *
     * @return
     */
    public double getNumeroLados() {
        return numeroLadosTriangulo;
    }

    /**
     * Método que calcula a área do triangulo.
     *
     * @return
     */
    public double calculaArea() {
        return (Math.sqrt(3) * (valorLado * valorLado)) / 4;

    }

    /**
     * Método que calcula os vertices do triangulo.
     */
    private void calculaVertices() {
        verticeY1[1] = verticeY1[0];
        verticeX1[1] = verticeX1[0] + this.valorLado;
        verticeY1[2] = verticeY1[0] + this.valorLado;
        verticeX1[2] = (verticeX1[0] + this.valorLado) / 2;

    }

    /**
     * Método que calcula o perimetro do triangulo.
     *
     * @return
     */
    public double calculaPerimetro() {
        return valorLado * 3;
    }

    /**
     * Metodo que imprime todos os dados gerados pelo calculo do poligono
     * triangulo.
     */
    public void imprimeTriangulo() {

        JOptionPane.showMessageDialog(null, "Número de lados da figura = " + getNumeroLados()
                + "\n\nVertice A = (" + verticeX1[0] + "," + verticeY1[0] + ")"
                + "\nVertice B = (" + verticeX1[1] + "," + verticeY1[1] + ")"
                + "\nVertice C = (" + verticeX1[2] + "," + verticeY1[2] + ")"
                + "\n\nAresta 1 = (" + verticeX1[0] + "," + verticeY1[0] + ") ao (" + verticeX1[1] + "," + verticeY1[1] + ")"
                + "\nAresta 2 = (" + verticeX1[1] + "," + verticeY1[1] + ") ao (" + verticeX1[2] + "," + verticeY1[2] + ")"
                + "\nAresta 3 = (" + verticeX1[2] + "," + verticeY1[2] + ") ao (" + verticeX1[0] + "," + verticeY1[0] + ")"
                + "\n\nÁrea =" + calculaArea()
                + "\n\nPerímetro = " + calculaPerimetro()
                + "\n\nCor da borda figura =" + getCorBordaTriangulo()
                + "\n\nCor da preenchimento figura =" + getCorPreenchimentoTriangulo(), "Triangulo", JOptionPane.INFORMATION_MESSAGE);
    }

}
