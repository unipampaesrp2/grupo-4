package rp2Figuras;

public abstract class Figurasgeometricas {

    Vertice[] vertice;
    Aresta[] aresta;
    int numLados;
    String corBorda, corPreenchimento;

    public Figurasgeometricas(int numeroLados) {
        numLados = numeroLados;
        aresta = new Aresta[numLados];
        vertice = new Vertice[numLados];
    }

    public abstract double area();

    public abstract double perimetro();
    
    
    public Vertice[] getConjuntoVerticeE() {
        return vertice;
     }

   
    public Aresta[] getConjuntoArestaE() {
        return aresta;
    }

    public String getConjuntoVertice() {
        String lista = "";
        for (int i = 0; i < numLados; i++) {
            lista = lista.concat("x = " + vertice[i].getX() + "  y = " + vertice[i].getY() + "\n");
        }
        return lista;
    }

    public String getConjuntoAresta() {
        String lista = "";
        for (int i = 0; i < numLados; i++) {
            lista = lista.concat("aresta " + (i + 1) + " ponto (" + aresta[i].getA().getX() + ", " + aresta[i].getA().getY() + ") ao (" + aresta[i].getB().getX() + ", " + aresta[i].getB().getY() + ")\n");
        }
        return lista;
    }

    public void calculaArestas() {
        for (int i = 0; i < numLados - 1; i++) {
            aresta[i] = new Aresta(vertice[i], vertice[i + 1]);

        }
        aresta[numLados - 1] = new Aresta(vertice[numLados - 1], vertice[0]);
    }

    //nao precisa ser abstrato pois a implementacao para todas as figuras vai ser a mesma, 
    // so vai retornar um atributo que ja foi adicionado.
    //para chamar = obj.getCorborda().
    public String getCorborda() {
        return corBorda;
    }

    public String getCorpreenchimento() {
        return corPreenchimento;
    }

    public int getNumeroladosfigura() {
        return numLados;
    }

    public void setCorBorda(String corBordaFigura) {
        this.corBorda = corBordaFigura;
    }

    public void setCorPreenchimento(String corPreenchimentoFigura) {
        this.corBorda = corPreenchimentoFigura;
    }

    public void setTranslacaoHorizontal(double x) {
        if (x > 0) {
            for (int i = 0; i < numLados - 1; i++) {
                vertice[i].setX(vertice[i].getX() + x);
            }
        } else {
            for (int i = 0; i < numLados - 1; i++) {
                vertice[i].setX(vertice[i].getX() - x);
            }
        }
    }

    public void setTranslacaoVertical(double y) {
        if (y > 0) {
            for (int i = 0; i < numLados - 1; i++) {
                vertice[i].setY(vertice[i].getY() + y);
            }
        } else {
            for (int i = 0; i < numLados - 1; i++) {
                vertice[i].setY(vertice[i].getY() - y);
            }
        }
    }

    public void setRotacaoFigura(double angulo) {
        double rad;
        double c, s, x, y;
        rad = (Math.PI / 180) * angulo;
        s = Math.sin(rad);
        c = Math.cos(rad);
        for (int i = 0; i < numLados; i++) {
            x = vertice[i].getX();
            y = vertice[i].getY();
            vertice[i].setX(x * c - y * s);
            vertice[i].setY(x * s + y * c);
        }
    }

    public void setDiminuirFigura(double sx, double sy) {
        for (int i = 0; i < numLados - 1; i++) {
            vertice[i].setX(vertice[i].getX() / sx);
            vertice[i].setY(vertice[i].getY() / sy);
        }
    }

    public void setAumentarFigura(double sx, double sy) {
        //sx fator de mudança escala horizontal.
        //sy fator de mudança escala vertical.
        for (int i = 0; i < numLados - 1; i++) {
            vertice[i].setX(vertice[i].getX() * sx);
            vertice[i].setY(vertice[i].getY() * sy);
        }
    }
    
    public abstract void calculaVertices(double x0, double y0);

}
