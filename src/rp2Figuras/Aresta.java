package rp2Figuras;

public class Aresta {

    private Vertice a, b;

    public Aresta(Vertice v1, Vertice v2) {
        a = v1;
        b = v2;
    }

    public Vertice getA() {
        return a;
    }

    public Vertice getB() {
        return b;
    }

    public void setA(Vertice v1) {
        a = v1;

    }

    public void setB(Vertice v1) {
        b = v1;

    }

}
