package rp2Figuras;

public class Vertice {

    private double x, y;

    public Vertice(double x0, double y0) {
        x = x0;
        y = y0;

    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x0) {
        x = x0;
    }

    public void setY(double y0) {
        y = y0;
    }
}
