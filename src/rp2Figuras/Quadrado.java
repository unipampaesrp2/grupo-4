package rp2Figuras;


/**
 * Classe com atributos do objeto quadrado.
 *
 * @author Camila
 */
public class Quadrado extends Figurasgeometricas {

    private double lado;

    public Quadrado(double x0, double y0, float lado) {
        super(4);
        calculaVertices(x0, y0);
        calculaArestas();
         
    }

    @Override
    public double area() {
        return (lado*lado);
    }

    @Override
    public double perimetro() {
        return (lado*4);
    }
    
    @Override
    public void calculaVertices(double x0, double y0) {
        vertice[0] = new Vertice(x0, y0);
        vertice[1] = new Vertice(x0 + lado, y0); 
        vertice[2] = new Vertice(x0 + lado, y0 + lado);
        vertice[3] = new Vertice(x0,y0 + lado);        
    }

    @Override
    public void setRotacaoFigura(double angulo) {
       double rad;
        double c, s, x, y;
        rad = (Math.PI / 180) * angulo;
        s = Math.sin(rad);
        c = Math.cos(rad);
        for (int i = 0; i < 4; i++) {
            x = vertice[i].getX();
            y = vertice[i].getY();
            vertice[i].setX(x * c - y * s);
            vertice[i].setY(x * s + y * c);

        }
    }
}
