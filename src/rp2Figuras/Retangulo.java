package rp2Figuras;

/**
 * Classe com atributos do objeto retângulo.
 *
 * @author Juliano
 */
public class Retangulo extends Figurasgeometricas {

    private double altura, base;

    public Retangulo(double x0, double y0, float lado, float h) {
        super(4);
        altura = h;
        base = lado;
        calculaVertices(x0, y0);
        calculaArestas();
    }

    @Override
    public double area() {
        return (base * altura);
    }

    @Override
    public double perimetro() {
        return (base + altura) * 2;
    }

    @Override
    public void calculaVertices(double x0, double y0) {
        vertice[0] = new Vertice(x0, y0);
        vertice[1] = new Vertice(x0 + base, y0);
        vertice[2] = new Vertice(x0 + base, y0 + altura);
        vertice[3] = new Vertice(x0, y0 + altura);
    }

}
