package rp2Figuras;

import javax.swing.JOptionPane;

/**
 * Classe para a execução do código.
 *
 * @author Juliano, Camila, Adriano.
 */
public class FigurasRP2 {

    /**
     * metodo para a execução dos métodos.
     *
     * @param args
     */
    public static void main(String[] args) {
        Retangulo r = new Retangulo(0, 0, 3, 4);
        JOptionPane.showMessageDialog(null, r.getConjuntoVertice());
       r.setRotacaoFigura(90);
       JOptionPane.showMessageDialog(null, r.getConjuntoAresta());
       
   
        
    }

}
