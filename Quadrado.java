package rp2Figuras;


/**
 * Classe com atributos do objeto quadrado.
 *
 * @author Camila
 */
public class Quadrado extends Figurasgeometricas {

    private double lado;

    public Quadrado(double x0, double y0, float valorLado) {
        super(4);
        this.lado=valorLado;
        calculaVertices(x0, y0);
        calculaArestas();
         
    }

    @Override
    public double area() {
        return (lado*lado);
    }

    @Override
    public double perimetro() {
        return (lado*4);
    }
    
    private void calculaVertices(double x0, double y0) {
        vertice[0] = new Vertice(x0, y0);
        vertice[1] = new Vertice(x0 + lado, y0); 
        vertice[2] = new Vertice(x0 + lado, y0 + lado);
        vertice[3] = new Vertice(x0,y0 + lado);        
    }

}
